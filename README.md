# Rust Graphics Playground

[Rust Graphics Playground](http://playground.meteorlinker.com) is a website on 
which you can write and share graphics demos in Rust that will run in the 
browser. Here are a few nice examples:

- [Spinning Triangle](http://playground.meteorlinker.com/?share=1)
- [Spinning Cube](http://playground.meteorlinker.com/?share=864)
- [Serpinski Triangle](http://playground.meteorlinker.com/?share=682)
- [Serpinski Carpet](http://playground.meteorlinker.com/?share=1054)


### Building and Running

While building the server code simply requires a Rust installation and a quick 
`cargo run --release`, please be aware that the server code will not work if 
`rustc` and `wasm-strip` are not available to the server in your PATH 
environment variable.


### How It Works

The code is quite straight-forward. There are 3 files that matter:

- server/src/main.rs
- server/src/template.rs
- page/script.js

The server application is sent requests containing the snippet written by the
user from the client-side Javascript, which it then inserts into the template's
`App::tick` function. This file is then build for release, targeting WASM,
stripped of excess symbols, and stored in place, to be retrieved by the
Javascript code and then set up and ran.

Each build attempt increments an ID counter, which is used to identify
combinations of compiled WASM libs, the snippet provided to build them, and the
full fusion of the template and the snippet.

The template file declares `#![no_std]` at the top, which introduces a couple
of awkward tasks (in exchange for vastly reducing the size of the compiled
lib). The first of these is that most maths functions aren't available in core
Rust, so trig functions, ceil, and floor have all been imported from the JS
side. We have to provide our own panic handler.  WASM doesn't have it's own way
to abort execution, and is run synchronously from the JS side, so the current
approach to handling panics is to call into a JS function that simply throws
an exception.

