// Write your code here. For context, you are inside a `&mut self` 
// method on a struct containing the following fields:
//
// tick:   u64        (the current tick)
// width:  usize      (the width of the canvas)
// height: usize      (the height of the canvas)
// buf:    &mut [u8]  (a buffer of the canvas' image data)
//
// Note that there are four u8 values per pixel in the buffer,
// for red, green, blue, and alpha channels (in that order).
//
// Also it's a no_std environment, so to make life easier some 
// JS maths functions have been imported: sin, cos, tan, ceil,
// floor, and round.

for y in 0..self.height {
    for x in 0..self.width {
        let idx = 4 * (x + y * self.width);

        // Shamelessly nabbed from http://cliffle.com/blog/bare-metal-wasm/
        self.buf[idx..idx+4].copy_from_slice(&[
          (self.tick.wrapping_add((x ^ y) as u64) & 0xFF) as u8,
          0,
          0,
          255,
        ]);
    }
}