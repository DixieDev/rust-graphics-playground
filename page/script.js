function init_onload() {
  // Grab a bunch of dynamic page elements
  const runBtn = document.querySelector('#run_button');
  const loadId = document.querySelector('#load_id');
  const loadBtn = document.querySelector('#load_button');
  const shareDiv = document.querySelector('#share_div');
  const shareSpan = document.querySelector('#share_span');
  const shareLink = document.querySelector('#share_a');
  const code = document.querySelector('#input');
  const canvas = document.querySelector('#output');
  const ctx = canvas.getContext('2d');
  const errorPanel = document.querySelector('#error_span');

  // Gonna use this to kill render loops
  let currentLoopId = 0;

  // Allow indent with tab
  code.addEventListener('keydown', function(e) {
    if (e.key == 'Tab') {
      e.preventDefault();
      var start = this.selectionStart;
      var end = this.selectionEnd;

      // set textarea value to: text before caret + tab + text after caret
      this.value = this.value.substring(0, start) +
        "\t" + this.value.substring(end);

      // put caret at right position again
      this.selectionStart =
        this.selectionEnd = start + 1;
    }
  });

  function handle_panic() {
    currentLoopId = 0;
    throw 'Panicked within Rust code!';
  }

  async function init_wasm(wasmId) {
    // Load in the WASM program
    const w = canvas.width;
    const h = canvas.height;

    const { instance } = await WebAssembly.instantiateStreaming(
      fetch('wasm/' + wasmId),
      {
        "env": {
          "js_handle_panic": handle_panic,
          "js_sin": Math.sin,
          "js_cos": Math.cos,
          "js_tan": Math.tan,
          "js_asin": Math.asin,
          "js_acos": Math.acos,
          "js_atan": Math.atan,
          "js_ceil": Math.ceil,
          "js_floor": Math.floor,
          "js_round": Math.round
        }
      }
    );
    const exports = instance.exports;

    // Init the app with our canvas dimensions
    const app = exports.init(w, h);
    try {
      exports.app_tick(app);
    } catch (e) {
      alert(e);
      return;
    }

    // Configure mapping between WASM memory and a JS ImageData, for use with the canvas
    const mem_addr = new Int32Array(exports.memory.buffer, exports.BUF.value)[0];
    const image = new ImageData(
      new Uint8ClampedArray(
        exports.memory.buffer,
        mem_addr,
        4*w*h,
      ),
      w,
    );

    // Increment current render loop, and set a new render loop
    currentLoopId += 1;
    const loopId = currentLoopId;
    const render = () => {
      // If this loop is meant to be the current one, tick and queue
      // up another tick for the next frame
      if (loopId == currentLoopId) {
        try {
          exports.app_tick(app);
          ctx.putImageData(image, 0, 0);
          requestAnimationFrame(render);
        } catch (e) {
          alert(e);
        }
      }
    };

    // Start the render loop
    render();
  }
  
  // Handle submitting code to the server
  runBtn.addEventListener('click', function() {
    const request = new XMLHttpRequest();
    
    request.addEventListener('loadend', function(event) {
      switch (request.status) {
        case 200:
          const id = request.response;
          shareSpan.textContent = 'Share Link: ';
          shareLink.href = 'http://playground.meteorlinker.com/?share=' + id;
          shareLink.textContent = 'http://playground.meteorlinker.com/?share=' + id;
          shareDiv.style.display = 'inline-block';
          errorPanel.style.display = 'none';
          init_wasm(id);
          break;
        case 400:
          errorPanel.style.display = 'block';
          errorPanel.textContent = request.response + 'Note: Line numbers are a bit janky, just subtract 124 from each one :)';
          break;
        default:
          alert('Something went wrong! Error code ' + request.status);
          break;
      }
    });

    request.addEventListener('error', function(event) {
      alert('Failed to send code to the server');
    });

    request.addEventListener('abort', function(event) {
      alert('Failed to send code to the server');
    });

    request.open('POST', 'submit');
    request.setRequestHeader('Content-Type', 'text/plain;charset=UTF-8');
    request.responseType = 'text';
    request.send(code.value);
  });

  // Load pre-existing program
  loadBtn.addEventListener('click', function() {
    const id = loadId.value;
    fetch ('rust/' + id)
      .then(response => response.text())
      .then(text => {
        shareDiv.style.display = 'none';
        code.value = text;
      });
    init_wasm(loadId.value);
  });

  // Initialise default program, maybe saving me some disk space :D
  const url = new URL(window.location.href);
  const shareId = url.searchParams.get("share");
  if (shareId == null) {
    init_wasm(0);
  } else {
    fetch ('rust/' + shareId)
      .then(response => response.text())
      .then(text => code.value = text);
    init_wasm(shareId);
  }
}

window.onload = init_onload;
