#![no_std]

#[panic_handler]
fn handle_panic(_: &core::panic::PanicInfo) -> ! {
    unsafe { js_handle_panic() }
}

use core::arch::wasm32;
const PAGE_SIZE: usize = 65536;

#[no_mangle]
pub static mut BUF: *mut u8 = core::ptr::null_mut();

// Import some JS maths functions, since they're not in core Rust...
extern {
    fn js_handle_panic() -> !;
    fn js_sin(x: f32) -> f32;
    fn js_cos(x: f32) -> f32;
    fn js_tan(x: f32) -> f32;
    fn js_asin(x: f32) -> f32;
    fn js_acos(x: f32) -> f32;
    fn js_atan(x: f32) -> f32;
    fn js_floor(x: f32) -> f32;
    fn js_ceil(x: f32) -> f32;
    fn js_round(x: f32) -> f32;
}

fn sin(x: f32) -> f32 { unsafe { js_sin(x) } }
fn cos(x: f32) -> f32 { unsafe { js_cos(x) } }
fn tan(x: f32) -> f32 { unsafe { js_tan(x) } }
fn asin(x: f32) -> f32 { unsafe { js_asin(x) } }
fn acos(x: f32) -> f32 { unsafe { js_acos(x) } }
fn atan(x: f32) -> f32 { unsafe { js_atan(x) } }
fn floor(x: f32) -> f32 { unsafe { js_floor(x) } }
fn ceil(x: f32) -> f32 { unsafe { js_ceil(x) } }
fn round(x: f32) -> f32 { unsafe { js_round(x) } }

#[no_mangle]
pub extern fn app_tick<'buf>(app: *mut App<'buf>) {
    unsafe {
        if let Some(app) = app.as_mut() {
            app.tick();
        }
    }
}

#[no_mangle]
pub extern fn init<'buf>(width: usize, height: usize) -> *mut App<'buf> {
    let buf_size = 4 * width * height + core::mem::size_of::<App>();
    let pages = buf_size / PAGE_SIZE + 1;
    let alloc_ptr = wasm32::memory_grow(0, pages) * PAGE_SIZE;

    unsafe { 
        BUF = alloc_ptr as *mut u8; 
        let slice_ptr = core::ptr::slice_from_raw_parts_mut(BUF, buf_size);

        // Make app on the stack
        let app = App {
            width,
            height,
            tick: 0,
            buf: match slice_ptr.as_mut() {
                Some(buf) => buf,
                None => loop {}, // Unreachable I hope :)
            },
        };

        // Copy stack App into explicity alloc'd memory
        let stack_app_ptr = (&app) as *const App<'buf> as *const u8;

        for i in 0..core::mem::size_of::<App>() {
            let slice_i = buf_size-core::mem::size_of::<App>() + i;
            *BUF.offset(slice_i as isize) = *stack_app_ptr.offset(i as isize);
        }

        let app_ptr = BUF as usize + buf_size-core::mem::size_of::<App>();
        app_ptr as *mut App<'buf>
    }
}

pub struct App<'buf> {
    tick: u64,
    width: usize,
    height: usize,
    buf: &'buf mut [u8],
}

impl<'buf> App<'buf> {
    fn tick(&mut self) {
        $$$
        self.tick += 1;
    }
}
