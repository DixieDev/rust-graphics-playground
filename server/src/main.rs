use rouille::{Request, Response};
use std::fs::{self, File};
use std::io::{Read, Write};
use std::process::Command;
use std::str::FromStr;
use std::sync::atomic::{AtomicU64, Ordering};

static RUST_TEMPLATE: &[u8] = include_bytes!("template.rs");
static NEXT_ID: AtomicU64 = AtomicU64::new(0);

fn main() {
    // Check for RGRAPH_PREFIX env variable, which specifies a common proefix on all API requests
    let prefix = std::env::var("RGRAPH_PREFIX").unwrap_or("".into());

    // Check for RGRAPH_PORT env variable
    let port = std::env::var("RGRAPH_PORT").unwrap_or("80".into());

    // Determine next ID in sequence, so we can continue where we left off
    let dir_iter = fs::read_dir("code/wasm")
        .unwrap()
        .filter_map(|entry| entry.ok());

    let mut next = 0;
    for entry in dir_iter {
        let path = entry.path();
        if !path.is_dir() {
            match path.file_name() {
                Some(s) => {
                    match s.to_str() {
                        Some(s) => {
                            let num_part = &s[0..s.len()-5];
                            match u64::from_str(num_part) {
                                Ok(n) if n > next => next = n,
                                _ => (),
                            }
                        },
                        None => (),
                    }
                },
                None => (),
            }
        }
    }

    NEXT_ID.store(next+1, Ordering::Relaxed);

    rouille::start_server(&format!("0.0.0.0:{}", port), move |request| { handler(request, &prefix) });
}

fn handler(request: &Request, prefix: &str) -> Response {
    let url = request.url();
    eprintln!("Received request: {}", url.as_str());

    let url = if url.starts_with(prefix) {
      url.split_at(prefix.len()).1
    } else {
      return empty_404()
    };

    let (path, content_type) = match url {
        "/submit"         => return process_submission(request),
        ""                => return Response::redirect_301(format!("{}/", prefix)),
        "/"               => ("index.html".to_string()    , "text/html"),
        "/stylesheet.css" => ("stylesheet.css".to_string(), "text/css"),
        "/script.js"      => ("script.js".to_string()     , "text/javascript"),
        url               => {
            if url.starts_with("/wasm/") {
                (format!("code/wasm/{}.wasm" , &url[6..]), "application/wasm")
            } else if url.starts_with("/rust/") {
                (format!("code/snip/{}.rs" , &url[6..]), "text/plain;charset=utf-8")
            } else {
                return empty_404();
            }
        }
    };

    let file = match File::open(&path) {
        Ok(file) => file,
        Err(_) => return empty_404(),
    };

    Response::from_file(content_type, file)
}

fn process_submission(request: &Request) -> Response {
    let mut body = match request.data() {
        Some(body) => body,
        None => return Response::empty_204(),
    };

    let id = gen_next_id();
    let rust_path = rust_path_from_id(&id);
    let snippet_path = snippet_path_from_id(&id);
    let wasm_path = wasm_path_from_id(&id);

    // Read Rust code from the request body
    let mut submitted_rust_code = String::new();
    match body.read_to_string(&mut submitted_rust_code) {
        Ok(_) => (),
        Err(_) => return empty_500(),
    }

    // Fit Rust code into template
    let rust_code = String::from_utf8_lossy(RUST_TEMPLATE);
    let rust_code = rust_code.replace("$$$", &submitted_rust_code);

    // Write Rust code to files. One combined with the template, one that's not. We erase the full
    // file once the compilation step has succeeded, and we remove both if compilation fails
    {
        let mut rust_file = match File::create(&rust_path) {
            Ok(file) => file,
            Err(_) => return empty_500(),
        };

        let mut snippet_file = match File::create(&snippet_path) {
            Ok(file) => file,
            Err(_) => return empty_500(),
        };
        
        match rust_file.write_all(&rust_code.as_bytes()) {
            Ok(_) => (),
            Err(_) => return empty_500(),
        };

        match snippet_file.write_all(&submitted_rust_code.as_bytes()) {
            Ok(_) => (),
            Err(_) => return empty_500(),
        }
    }
    
    // Compile Rust to WASM
    let compile_output = Command::new("rustc")
        .args(&[
            "--crate-type", "cdylib",  
            "--target", "wasm32-unknown-unknown",
            "-C", "opt-level=3",
            "-o", &wasm_path,
            &rust_path
        ])
        .output();

    // Remove source file
    fs::remove_file(&rust_path).ok();

    // Check compilation result
    match compile_output {
        Ok(output)  if !output.status.success() => {
            fs::remove_file(&snippet_path).ok();
            return Response::from_data("text/plain;charset=utf-8", output.stderr)
                .with_status_code(400);
        },
        Ok(_) => (),
        Err(_) => {
            fs::remove_file(&snippet_path).ok();
            return empty_500();
        }
    }

    // Strip wasm binary
    Command::new("wasm-strip")
        .arg(&wasm_path)
        .spawn()
        .unwrap()
        .wait()
        .unwrap();

    // Respond with the program ID
    return Response::from_data("text/plain;charset=utf-8", id)
        .with_status_code(200); 
}

fn gen_next_id() -> String {
    format!("{}", NEXT_ID.fetch_add(1, Ordering::Relaxed))
}

fn rust_path_from_id(id: &str) -> String {
    format!("code/rust/{}.rs", id)
}

fn snippet_path_from_id(id: &str) -> String {
    format!("code/snip/{}.rs", id)
}

fn wasm_path_from_id(id: &str) -> String {
    format!("code/wasm/{}.wasm", id)
}

fn empty_404() -> Response { Response::empty_404() }
fn empty_500() -> Response { Response::empty_400().with_status_code(500) } // lol
